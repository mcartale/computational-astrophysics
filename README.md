# Computational Astrophysics

## Script for reading the merger trees 
script_example.py: for reading the full file of Sublink

## For the MW-type galaxy
GalaxySample_Gnr244Sgrn547844_TNG50_snapshot99.h5: MW-type galaxy from TNG50

Read_MW.ipynb: jupyter notebook for reading the MW-type galaxy file 
