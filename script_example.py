import sys
sys.path.append('/home/celeste/illustris_python/') #This is one option to add the module to PYTHONPATH. Please change
import illustris_python as il
import numpy as np
import matplotlib
matplotlib.use('Agg')
import h5py
import time

basePath = '/TNG100-1/output/' #Please change the basePath accordingly

###PARAMETERS####
h = 0.6774 #hubble
gal_mass_min = 1e7 #min stellar mass
gal_mass_max = 1e12 #max stellar mass
sfr_min = 0.0001 #min SFR
sfr_max = 10000 #max SFR
snapshot = 99 #snapshot number 99 represents z=0 for TNG
N_chosen = 'all'#selecting all the branches (not only the MB)
catalogue_filename = 'IllustrisTNG100-MergerTrees_Ms'+str(gal_mass_min)+'-'+str(gal_mass_max)+'_SFR'+str(sfr_min)+'-'+str(sfr_max)+'_N'+str(N_chosen)+'_'+str(snapshot)+'.hdf5'
################

#Read Galaxy Properties and select the ones needed at z=0 (snapshot number 99)#
subhalos = il.groupcat.loadSubhalos(basePath,snapshot,fields=['SubhaloMassType','SubhaloSFR','SubhaloGasMetallicitySfr'])
Ms = np.array(subhalos['SubhaloMassType'][:,4]*1e10/h)#Stellar mass: Msol
SFR = np.array(subhalos['SubhaloSFR'])#Star formation rate: Msol/yr
Zmet = np.array(subhalos['SubhaloGasMetallicitySfr'])#SF_gas Metallicity

#condition for stellar mass and SFR of selected galaxies#
subhalo_id = np.where((Ms>=gal_mass_min)&(Ms<=gal_mass_max)&(SFR>=sfr_min)&(SFR<sfr_max))[0]

#fields that will be extracted from the merger trees#
fields = ['SubfindID','SnapNum','SubhaloGrNr','SubhaloMassType','SubhaloSFR','SubhaloGasMetallicitySfr']
fw = h5py.File(catalogue_filename, 'w')

#First test is a loop for the first 3 subhaloes/galaxies#
for i in range(0,3):
    print 'subhaloid',subhalo_id[i]
    #reads the merger tree for the galaxy subhalo subhalo_id[i]#
    tree = il.sublink.loadTree(basePath,snapshot,subhalo_id[i],fields=fields,onlyMPB=False)# onlyMPB=False indicates it will collect all the branches
    if(tree):
        #created group
        grp = fw.create_group("GalaxyID_"+str(subhalo_id[i]))
        dset_Redshift = grp.create_dataset("SnapNum", data=tree['SnapNum'])
        dset_StellarMass = grp.create_dataset("MassType_Star", data=tree['SubhaloMassType'][:,4]*1e10/h)#In Msol
        dset_SFR = grp.create_dataset("StarFormationRate", data=tree['SubhaloSFR']) #Msol/yr
        dset_GalaxyID = grp.create_dataset("SubfindID", data=tree['SubfindID'])
        dset_Zmet = grp.create_dataset("SF_Metallicity", data=tree['SubhaloGasMetallicitySfr'])


fw.close()
